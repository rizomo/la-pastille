import { SnapWidget } from '../lib/widget';

export enum WidgetEventType {
  NOTIFICATION = 'snap-notification',
  AUTH = 'snap-auth',
  OPEN = 'snap-open',
  CLOSE = 'snap-close',
  FULLSCREEN = 'snap-fullscreen',
  FULLSCREEN_EXIT = 'snap-fullscreen-exit',
  READY = 'main-frame-ready',
}

export interface WidgetEventMap extends HTMLElementEventMap {
  [WidgetEventType.AUTH]: WidgetAuthEvent;
  [WidgetEventType.CLOSE]: WidgetCloseEvent;
  [WidgetEventType.OPEN]: WidgetOpenEvent;
  [WidgetEventType.FULLSCREEN]: WidgetFullScreenEvent;
  [WidgetEventType.FULLSCREEN_EXIT]: WidgetFullScreenExitEvent;
  [WidgetEventType.NOTIFICATION]: WidgetNotificationEvent;
  [WidgetEventType.READY]: WidgetReadyEvent;
}

export class WidgetEvent extends Event {
  widget: SnapWidget;

  constructor(
    widget: SnapWidget,
    type: WidgetEventType,
    eventInitDict?: EventInit | undefined
  ) {
    super(type, eventInitDict);

    this.widget = widget;
  }
}

export class WidgetReadyEvent extends WidgetEvent {
  constructor(widget: SnapWidget, eventInitDict?: EventInit | undefined) {
    super(widget, WidgetEventType.READY, eventInitDict);
  }
}

export class WidgetOpenEvent extends WidgetEvent {
  constructor(widget: SnapWidget, eventInitDict?: EventInit | undefined) {
    super(widget, WidgetEventType.OPEN, eventInitDict);
  }
}

export class WidgetCloseEvent extends WidgetEvent {
  constructor(widget: SnapWidget, eventInitDict?: EventInit | undefined) {
    super(widget, WidgetEventType.CLOSE, eventInitDict);
  }
}

export class WidgetFullScreenEvent extends WidgetEvent {
  constructor(widget: SnapWidget, eventInitDict?: EventInit | undefined) {
    super(widget, WidgetEventType.FULLSCREEN, eventInitDict);
  }
}

export class WidgetFullScreenExitEvent extends WidgetEvent {
  constructor(widget: SnapWidget, eventInitDict?: EventInit | undefined) {
    super(widget, WidgetEventType.FULLSCREEN_EXIT, eventInitDict);
  }
}

export class WidgetAuthEvent extends WidgetEvent {
  /**
   * @param state `true`: The user is connected - `false`: The user is disconnected
   */
  authState: boolean;

  constructor(
    widget: SnapWidget,
    authState: boolean,
    eventInitDict?: EventInit | undefined
  ) {
    super(widget, WidgetEventType.AUTH, eventInitDict);

    this.authState = authState;
  }
}

export class WidgetNotificationEvent extends WidgetEvent {
  notificationsCount: number;

  constructor(
    widget: SnapWidget,
    notificationsCount: number,
    eventInitDict?: EventInit | undefined
  ) {
    super(widget, WidgetEventType.NOTIFICATION, eventInitDict);

    this.notificationsCount = notificationsCount;
  }
}

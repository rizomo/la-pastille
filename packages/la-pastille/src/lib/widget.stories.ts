// Button.stories.ts

import type { Meta, StoryObj } from '@storybook/web-components';
import './widget.ts';

import { html } from 'lit';

const meta: Meta = {
  component: 'snap-widget',
};
export default meta;

type Story = StoryObj;

export const Playground: Story = {
  render: () =>
    html`<snap-widget
      open="true"
      url="https://rizomo-preprod.numerique.gouv.fr"
    ></snap-widget>`,
};

export const Frame: Story = {
  render: () =>
    html`<iframe
      src="https://rizomo-preprod.numerique.gouv.fr"
      style="position: absolute;inset: 0;border: 0;width: 100%;height: 100%;"
    ></iframe>`,
};

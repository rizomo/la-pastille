import inlineStyles from '../style.module.scss?inline';
import styles from '../style.module.scss';
import {
  WidgetAuthEvent,
  WidgetCloseEvent,
  WidgetEventMap,
  WidgetEventType,
  WidgetFullScreenEvent,
  WidgetFullScreenExitEvent,
  WidgetNotificationEvent,
  WidgetOpenEvent,
  WidgetReadyEvent,
} from '../events/WidgetEvent';
import { WidgetButton } from './components/WidgetButton';
import {
  Message,
  MessageType,
  Question,
  QuestionType,
  SnapWidgetOptions,
} from './types';
import { Tab, WidgetFrame } from './components/WidgetFrame';

// TODO: Add file upload support

// TODO: Add countly event support

export class SnapWidget {
  private _styleSheet: CSSStyleSheet;

  private _container: HTMLDivElement;
  private _loadFrame: HTMLIFrameElement;
  private _widgetButton: WidgetButton;
  private _widgetFrame: WidgetFrame;

  private _appUrl: string;
  private _isOpen: boolean;
  private _isFullscreen: boolean;
  private _isMainFrameReady = false;

  private _isAuthenticated: boolean = false;
  private _notificationsCount: number = 0;

  constructor(options?: Partial<SnapWidgetOptions>) {
    // Initialize state
    this._appUrl = options?.appUrl ?? 'https://rizomo.numerique.gouv.fr';
    this._isOpen = options?.defaultOpen ?? false;
    this._isFullscreen = options?.defaultFullscreen ?? false;

    this.handleMessage = this.handleMessage.bind(this);

    // Initialize container
    this._container = document.createElement('div');
    this._container.classList.add(styles.container);

    this._loadFrame = document.createElement('iframe');
    this._loadFrame.classList.add(styles.loaderFrame);
    this._loadFrame.loading = 'eager';
    this._loadFrame.name = 'loaderFrame';
    this._loadFrame.src = `${this._appUrl}/widget/load`;

    // Initialize components
    this._widgetButton = new WidgetButton(this);
    this._widgetFrame = new WidgetFrame(this);

    // Append components to the container
    this._container.append(this._loadFrame);
    this._container.append(this._widgetButton.element);
    this._container.append(this._widgetFrame.element);

    // Initialize style
    this._styleSheet = new CSSStyleSheet();
    this._styleSheet.replaceSync(inlineStyles);

    // Initialize envent handlers
    window.addEventListener('message', this.handleMessage, false);

    this._loadFrame.addEventListener('load', () => {
      this.load();
    });
  }

  get styleSheet() {
    return this._styleSheet;
  }

  get container() {
    return this._container;
  }

  get appUrl() {
    return this._appUrl;
  }
  get isOpen() {
    return this._isOpen;
  }
  get isFullscreen() {
    return this._isFullscreen;
  }
  get isAuthenticated() {
    return this._isAuthenticated;
  }
  get notificationsCount() {
    return this._notificationsCount;
  }

  get isMainFrameReady() {
    return this._isMainFrameReady;
  }

  get mainFrame() {
    return this._widgetFrame.getTabFrame(Tab.RIZOMO)!;
  }

  public addEventListener<K extends keyof WidgetEventMap>(
    type: K,
    listener: (this: HTMLDivElement, ev: WidgetEventMap[K]) => any,
    options?: boolean | AddEventListenerOptions
  ): void;
  public addEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | AddEventListenerOptions
  ): void {
    this.container.addEventListener(type, listener, options);
  }

  public removeEventListener<K extends keyof WidgetEventMap>(
    type: K,
    listener: (this: HTMLDivElement, ev: WidgetEventMap[K]) => any,
    options?: boolean | EventListenerOptions
  ): void;
  public removeEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | EventListenerOptions
  ): void {
    this.container.removeEventListener(type, listener, options);
  }

  public open() {
    this._isOpen = true;
    this.container.setAttribute('data-open', 'true');
    this.container.dispatchEvent(new WidgetOpenEvent(this));
  }
  public close() {
    this._isOpen = false;
    this.container.setAttribute('data-open', 'false');
    this.container.dispatchEvent(new WidgetCloseEvent(this));
  }

  public enterFullscreen() {
    this._isFullscreen = true;
    this.container.setAttribute('data-fullscreen', 'true');
    this.container.dispatchEvent(new WidgetFullScreenEvent(this));
  }
  public exitFullscreen() {
    this._isFullscreen = false;
    this.container.setAttribute('data-fullscreen', 'false');
    this.container.dispatchEvent(new WidgetFullScreenExitEvent(this));
  }
  public toggleFullscreen() {
    if (this._isFullscreen) {
      this.exitFullscreen();
    } else {
      this.enterFullscreen();
    }
  }

  public loadMainFrame() {
    if (this.isMainFrameReady) return;

    const frameDocument =
      this.mainFrame?.contentDocument ||
      this.mainFrame?.contentWindow?.document;

    this.mainFrame.setAttribute('loading', 'eager');

    return new Promise<void>((resolve, reject) => {
      const onReady = () => {
        resolve();
      };
      this.addEventListener(WidgetEventType.READY, onReady);

      setTimeout(() => {
        reject(new Error('Loading timed out'));
        this.removeEventListener(WidgetEventType.READY, onReady);
      }, 30000);
    });
  }

  /**
   * @param state `true`: The user is connected - `false`: The user is disconnected
   */
  public authenticate(state: true | false) {
    this._isAuthenticated = state;
    this.container.dispatchEvent(new WidgetAuthEvent(this, state));
  }

  public notify(notificationsCount: number) {
    this._notificationsCount = notificationsCount;
    this.container.dispatchEvent(
      new WidgetNotificationEvent(this, notificationsCount)
    );
  }

  public load() {
    this._loadFrame.contentWindow?.postMessage('load', '*');
  }

  private ready() {
    this._isMainFrameReady = true;
    this.container.dispatchEvent(new WidgetReadyEvent(this));
  }

  private handleMessage(event: MessageEvent<Message | Question>) {
    const data = event.data;

    const callback = (cbData?: any) => {
      if ('callback' in data && data.callback) {
        this.mainFrame?.contentWindow?.postMessage(
          { type: 'callback', callback: data.callback, data: cbData },
          '*'
        );
      }
    };

    switch (data.type) {
      case MessageType.NOTIFICATION:
        this.notify(Number(data.content));
        callback();
        break;
      case MessageType.AUTH:
        this.authenticate(Boolean(data.content));
        callback();
        break;
      case MessageType.READY:
        this.ready();
        callback();
        break;

      case MessageType.LOAD:
        this.notify(Number(data.content?.notifications));
        this.authenticate(Boolean(data.content?.authenticated));
        break;
      case 'isWiget':
        callback(true);
        break;
      case 'isFullScreen':
        callback(this.isFullscreen);
        break;
      case 'isOpened':
        callback(this.isOpen);
        break;
      case 'openWidget':
        this.open();
        callback();
        break;
      case 'closeWidget':
        this.close();
        callback();
        break;
      case 'setFullScreen':
        if (data.content === true) {
          this.enterFullscreen();
        } else if (data.content === false) {
          this.exitFullscreen();
        }
        callback();
        break;

      default:
        break;
    }
  }

  /**
   *
   * @param elementId
   */
  public attach(element?: string | HTMLElement) {
    let el: HTMLElement;
    if (element) {
      if (element instanceof HTMLElement) {
        el = element;
      } else {
        const elementById = document.getElementById(element);

        if (!elementById) throw new Error('Element not found');

        el = elementById;
      }
    } else {
      el = document.body;
    }

    el.appendChild(this.container);

    document.adoptedStyleSheets = [
      ...document.adoptedStyleSheets,
      this.styleSheet,
    ];
  }
}

export class SnapWidgetElement extends HTMLElement {
  widget: SnapWidget;

  constructor() {
    super();

    this.widget = new SnapWidget({
      appUrl: this.getAttribute('url') || undefined,
      defaultFullscreen: this.getAttribute('fullscreen') === 'true',
      defaultOpen: this.getAttribute('open') === 'true',
    });

    this.widget.addEventListener(WidgetEventType.OPEN, () => {
      this.setAttribute('open', 'true');
    });
    this.widget.addEventListener(WidgetEventType.CLOSE, () => {
      this.setAttribute('open', 'false');
    });
    this.widget.addEventListener(WidgetEventType.FULLSCREEN, () => {
      this.setAttribute('fullscreen', 'true');
    });
    this.widget.addEventListener(WidgetEventType.FULLSCREEN_EXIT, () => {
      this.setAttribute('fullscreen', 'false');
    });

    if ('attachShadow' in this) {
      const shadow = this.attachShadow({ mode: 'open' });

      shadow.appendChild(this.widget.container);
      shadow.adoptedStyleSheets = [this.widget.styleSheet];
    } else {
      console.warn('Browser does not support ShadowDOM. Some bugs may occur.');

      (this as SnapWidgetElement).widget.attach(this as SnapWidgetElement);
    }
  }

  static get observedAttributes() {
    return ['open', 'fullscreen'] as const;
  }

  attributeChangedCallback(
    name: typeof SnapWidgetElement.observedAttributes[number],
    oldValue: string,
    newValue: string
  ) {
    switch (name) {
      case 'open':
        if (newValue === 'true') {
          if (!this.widget.isOpen) {
            this.widget.open();
          }
        } else {
          if (this.widget.isOpen) {
            this.widget.close();
          }
        }
        break;
      case 'fullscreen':
        if (newValue === 'true') {
          if (!this.widget.isFullscreen) {
            this.widget.enterFullscreen();
          }
        } else {
          if (this.widget.isFullscreen) {
            this.widget.exitFullscreen();
          }
        }
        break;
    }
  }
}

// Check if code is executed in the browser
if (typeof window !== 'undefined') {
  // Check if browser supports WebComponents
  if ('customElements' in window) {
    // Init WebComponents
    window.customElements.define('snap-widget', SnapWidgetElement);
  }
}

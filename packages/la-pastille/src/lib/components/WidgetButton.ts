import { SnapWidget } from '../widget';
import styles from '../../style.module.scss';
import { WidgetEvent, WidgetEventType } from '../../events/WidgetEvent';

export class WidgetButton {
  private _widget: SnapWidget;

  private btn: HTMLButtonElement;
  private image: HTMLImageElement;
  private notifications: HTMLSpanElement;

  private posX = 0;
  private posY = 0;
  private mouseX = 0;
  private mouseY = 0;

  private _isDragging = false;
  private _isDropping = false;

  constructor(widget: SnapWidget) {
    this._widget = widget;

    this.btn = document.createElement('button');
    this.btn.classList.add(styles.widget);
    this.btn.title = 'Accéder aux services réservés aux agents de l’Etat';

    this.notifications = document.createElement('span');
    this.notifications.classList.add(styles.notifications);

    this.image = document.createElement('img');
    this.image.setAttribute(
      'src',
      `${widget.appUrl}/widget/assets/${
        widget.isAuthenticated ? 'connected' : 'disconnected'
      }`
    );

    this.btn.appendChild(this.image);
    this.btn.appendChild(this.notifications);

    this.onPointerDown = this.onPointerDown.bind(this);
    this.onPointerUp = this.onPointerUp.bind(this);
    this.onPointerMove = this.onPointerMove.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.setNotification = this.setNotification.bind(this);
    this.setAuth = this.setAuth.bind(this);

    this.btn.addEventListener('pointerdown', this.onPointerDown);

    this.btn.addEventListener('click', (event) => {
      event?.preventDefault();
      if (!this._isDragging) {
        widget.open();
      }
    });

    this.btn.addEventListener('contextmenu', (event) => {
      event?.preventDefault();
      widget.open();
    });

    this.btn.addEventListener('dragover', (event) => event.preventDefault());
    this.btn.addEventListener('dragenter', (event) => this.setDropping(true));
    this.btn.addEventListener('dragleave', (event) => this.setDropping(false));
    this.btn.addEventListener('drop', this.onDrop);

    widget.addEventListener(WidgetEventType.AUTH, (event) => {
      this.setAuth(event.authState);
    });

    widget.addEventListener(WidgetEventType.NOTIFICATION, (event) => {
      this.setNotification(event.notificationsCount);
    });
  }

  get isDragging() {
    return this._isDragging;
  }

  get isDropping() {
    return this._isDropping;
  }

  get widget() {
    return this._widget;
  }

  private setDragging(isDragging: boolean) {
    this._isDragging = isDragging;

    if (isDragging) {
      this.element.classList.add(styles.dragging);
    } else {
      this.element.classList.remove(styles.dragging);
    }
  }

  private setDropping(isDropping: boolean) {
    this._isDropping = isDropping;

    if (isDropping) {
      this.element.classList.add(styles.dropping);
    } else {
      this.element.classList.remove(styles.dropping);
    }
  }

  private onPointerDown(event: PointerEvent) {
    event.preventDefault();
    if (event.button === 0) {
      this.posX = event.clientX - this.btn.offsetLeft;
      this.posY = event.clientY - this.btn.offsetTop;
      this.btn.addEventListener('pointermove', this.onPointerMove, false);
      document.addEventListener('pointerup', this.onPointerUp, { once: true });

      this.btn.setPointerCapture(event.pointerId);
    }

    if (event.button === 1) {
      this.resetPosition();
    }
  }

  private onPointerUp(event: PointerEvent) {
    this.btn.removeEventListener('pointermove', this.onPointerMove, false);

    this.btn.releasePointerCapture(event.pointerId);

    window.requestAnimationFrame(() => {
      this.setDragging(false);
    });
  }

  private onPointerMove(event: PointerEvent) {
    event.preventDefault();
    this.mouseX = event.clientX - this.posX;
    this.mouseY = event.clientY - this.posY;
    this.updateButtonPosition(this.mouseX, this.mouseY);

    // Update the flag to indicate that a drag is in progress
    this.setDragging(true);
  }

  private async onDrop(event: DragEvent) {
    // Prevent default behavior (Prevent file from being opened)
    event.preventDefault();

    await this.widget.loadMainFrame();

    const files: File[] = [];

    const dt = event.dataTransfer;
    if (dt) {
      if (dt.items) {
        // Use DataTransferItemList interface to access the file(s)
        // @ts-ignore
        [...dt.items].forEach((item) => {
          // If dropped items aren't files, reject them
          if (item.kind === 'file') {
            files.push(item.getAsFile());
          }
        });
      } else {
        // Use DataTransfer interface to access the file(s)
        // @ts-ignore
        [...dt.files].forEach((file) => {
          files.push(file);
        });
      }
    }

    this.widget.open();
    this.widget.enterFullscreen();

    this.widget.mainFrame?.contentWindow?.postMessage(
      { type: 'widget', event: 'upload', files },
      '*'
    );

    this.setDropping(false);
  }

  public updateButtonPosition(x: number, y: number) {
    if (x < 0 || x > window.innerWidth - this.btn.clientWidth) {
      if (x < 0) {
        this.btn.style.left = '0px';
      } else {
        this.btn.style.left = window.innerWidth - this.btn.clientWidth + 'px';
      }
    } else {
      this.btn.style.left = x + 'px';
    }

    if (y < 0 || y > window.innerHeight - this.btn.clientHeight) {
      if (y < 0) {
        this.btn.style.top = '0px';
      } else {
        this.btn.style.top = window.innerHeight - this.btn.clientHeight + 'px';
      }
    } else {
      this.btn.style.top = y + 'px';
    }
  }

  public setAuth(isAuthenticated: boolean) {
    this.image.setAttribute(
      'src',
      `${this.widget.appUrl}/widget/assets/${
        isAuthenticated ? 'connected' : 'disconnected'
      }`
    );
  }

  public setNotification(notificationsCount: number) {
    if (notificationsCount === 0) {
      this.notifications.innerText = '';
    } else {
      this.notifications.innerText = notificationsCount.toString();
    }
  }

  public resetPosition() {
    this.posX = 0;
    this.posY = 0;
    this.btn.style.top = '';
    this.btn.style.left = '';
  }

  get element() {
    return this.btn;
  }
}

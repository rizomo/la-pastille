import { SnapWidget } from '../widget';
import styles from '../../style.module.scss';
import { createElement } from '../utils/render';
import { NavItem } from '../types';
import { WidgetEventType } from '../../events/WidgetEvent';

import MessageIcon from '../../icons/message.svg?raw';
import CloseIcon from '../../icons/close.svg?raw';
import FullscreenIcon from '../../icons/fullscreen.svg?raw';
import FullscreenExitIcon from '../../icons/fullscreen-exit.svg?raw';

export enum Tab {
  RIZOMO = 'rizomo',
  CHATBOT = 'chatbot',
}

const TABS: NavItem[] = [
  {
    key: Tab.RIZOMO,
    iframe: (widget) =>
      createElement('iframe', {
        id: 'lb_iframe-widget',
        name: 'lb_iframe-widget',
        src: widget.appUrl,
        allow: 'fullscreen',
        style: {
          border: '0',
        },
      }),
    icon: (widget) =>
      createElement('img', {
        src: `${widget.appUrl}/widget/assets/logo`,
        style: {
          borderRadius: '4px',
        },
      }),
  },
  {
    key: Tab.CHATBOT,
    iframe: (widget) =>
      createElement('iframe', {
        id: 'chatbot-frame',
        name: 'chatbot-frame',
        src: `${widget.appUrl}/chat`,
        allow: 'fullscreen autoplay',
        style: {
          border: '0',
        },
      }),
    icon: (widget) => {
      const chatIcon = document.createElement('i');
      chatIcon.innerHTML = MessageIcon;

      return chatIcon;
    },
  },
];

const defaultTab = Tab.RIZOMO;

export class WidgetFrame {
  private _widget: SnapWidget;

  private frame: HTMLDivElement;

  private _tab: Tab = defaultTab;
  private _tabFrames = new Map<Tab, HTMLIFrameElement>();
  private _tabBtns = new Map<Tab, HTMLButtonElement>();

  constructor(widget: SnapWidget) {
    this._widget = widget;

    this.onCloseClick = this.onCloseClick.bind(this);
    this.onFullScreenToggleClick = this.onFullScreenToggleClick.bind(this);
    this.onNavClick = this.onNavClick.bind(this);

    this.frame = createElement(
      'div',
      {
        class: styles.frame,
      },
      [
        createElement(
          'header',
          {
            class: styles.header,
          },
          [
            createElement(
              'nav',
              {
                class: styles.nav,
              },
              TABS.map((tab) => {
                const button = createElement(
                  'button',
                  {
                    class: styles.navBtn,
                  },
                  tab.icon(widget)
                );

                if (tab.title && tab.showTitle) {
                  const title = document.createElement('span');
                  title.classList.add(styles.navTitle);
                  title.innerText = tab.title;

                  button.append(title);
                }

                button.addEventListener('click', () => this.changeTab(tab.key));
                this._tabBtns.set(tab.key, button);

                return button;
              })
            ),
            createElement(
              'div',
              {
                class: styles.actions,
              },
              [
                () => {
                  const button = document.createElement('button');
                  button.title = 'Activer / Desactiver le mode plein écran';
                  button.classList.add(styles.actionBtn);
                  button.innerHTML = widget.isFullscreen
                    ? FullscreenExitIcon
                    : FullscreenIcon;

                  button.addEventListener(
                    'click',
                    this.onFullScreenToggleClick,
                    false
                  );

                  widget.addEventListener(WidgetEventType.FULLSCREEN, () => {
                    button.innerHTML = FullscreenExitIcon;
                  });
                  widget.addEventListener(
                    WidgetEventType.FULLSCREEN_EXIT,
                    () => {
                      button.innerHTML = FullscreenIcon;
                    }
                  );

                  return button;
                },
                () => {
                  const button = document.createElement('button');
                  button.title = 'Fermer la pastille';
                  button.classList.add(styles.actionBtn);
                  button.innerHTML = CloseIcon;
                  button.addEventListener('click', this.onCloseClick, false);

                  return button;
                },
              ]
            ),
          ]
        ),
        createElement(
          'div',
          {
            class: styles.content,
          },
          TABS.map((tab) => {
            const iframe = tab.iframe(widget);
            iframe.loading = 'lazy';
            iframe.classList.add(styles.iframe);
            iframe.setAttribute('data-tab', tab.key);
            if (tab.key === this._tab) {
              iframe.setAttribute('data-tab-active', 'true');
            }

            this._tabFrames.set(tab.key, iframe);

            return iframe;
          })
        ),
      ]
    );
  }

  private onCloseClick(event: MouseEvent) {
    this._widget.close();
  }

  private onFullScreenToggleClick(event: MouseEvent) {
    this._widget.toggleFullscreen();
  }

  private onNavClick(event: MouseEvent) {}

  public changeTab(tab: Tab) {
    const prevBtn = this._tabBtns.get(this._tab);
    const prevFrame = this._tabFrames.get(this._tab);

    this._tab = tab;

    const newBtn = this._tabBtns.get(tab);
    const newFrame = this._tabFrames.get(tab);

    prevBtn?.setAttribute('data-tab-active', 'false');
    prevFrame?.setAttribute('data-tab-active', 'false');

    newBtn?.setAttribute('data-tab-active', 'true');
    newFrame?.setAttribute('data-tab-active', 'true');
  }

  get tab() {
    return this._tab;
  }

  public getTabFrame(tab: Tab) {
    return this._tabFrames.get(tab);
  }
  public getTabButton(tab: Tab) {
    return this._tabBtns.get(tab);
  }

  get widget() {
    return this._widget;
  }

  get element() {
    return this.frame;
  }
}

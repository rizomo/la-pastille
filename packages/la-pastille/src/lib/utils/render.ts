type HTMLAttributes<T extends HTMLElement> = Partial<
  Omit<Pick<T, keyof T>, 'style'> &
    Record<string, any> & { style: Partial<CSSStyleDeclaration> }
>;

type RenderNode = HTMLElement | (() => RenderNode) | RenderNode[];

export function createElement<K extends keyof HTMLElementTagNameMap>(
  tagName: K,
  attributes?: HTMLAttributes<HTMLElementTagNameMap[K]>,
  childrens?: RenderNode
): HTMLElementTagNameMap[K] {
  const element = document.createElement(tagName) as HTMLElementTagNameMap[K];

  if (attributes && typeof attributes === 'object') {
    if ('style' in attributes && attributes.style) {
      for (const [styleKey, styleValue] of Object.entries(attributes.style)) {
        // @ts-ignore
        element.style[styleKey] = styleValue;
      }
    }

    for (const [key, value] of Object.entries(attributes)) {
      if (
        typeof value === 'string' ||
        typeof value === 'number' ||
        typeof value === 'boolean'
      ) {
        element.setAttribute(key, value.toString());
      }
    }
  }

  if (childrens) {
    element.append(...render(childrens));
  }

  return element;
}

export function render(childrens: RenderNode): HTMLElement[] {
  if (!childrens) {
    return [];
  }

  if (Array.isArray(childrens)) {
    return childrens.flatMap(render);
  } else {
    if (typeof childrens === 'function') {
      return render(childrens());
    } else {
      return [childrens];
    }
  }
}

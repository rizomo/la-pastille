# la-pastille

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build la-pastille` to build the library.

## Running unit tests

Run `nx test la-pastille` to execute the unit tests via [Jest](https://jestjs.io).
